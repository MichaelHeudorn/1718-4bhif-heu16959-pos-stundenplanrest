# README #

## What is the aim of this project? 
The Aim of the project is to provide JSON-Data for an Android App which I am going to develope later on.  
The JSON-Data provides Timetable-Data from a SQL-Server.  
  
## Technical Specs
For this project I am using Spring-Boot, Spring-Data, Lombok and JPA. It is based on Java-1.8 (maybe I will upgrade  to Java 10 later).  
The provided data is fetched from a SQL-Server (2017).
  
## Architectural Pattern
The Pattern this project follows is MVC (Model View Controler). 
  
## Requirements
To run the final war file you will need to have Java-1.8 installed and a running SQL-Server 2017 with the existing timetable database. 
  
## Database Diagram
![Imgur Image](https://i.imgur.com/WH4Mgfw.png)
  
## URL Pattern
Following to the address where the Server is hosted (e.g. localhost) there are following properties:  
  
E.g. using localhost:  
	*localhost:8080/Faecher*  
	*localhost:8080/Klassen*  
	*localhost:8080/Stundenplan*  
	*localhost:8080/Lehrer*  

You can also use the following pattern to search for e.g. ID's:  
	*localhost:8080/Stundenplan/Lehrer/{id}*  
	Where {id} has to be a id of a teacher.  